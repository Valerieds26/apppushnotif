import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public currentUser: any;
  private token: string;
  private registrationId: string;

  constructor(public navCtrl: NavController, private http: Http) {
  }

   /**
     * Custom login with an account stored
     */
    public customLogin(): void {
          this.currentUser = {email: 'blancacho.jonathan@hotmail.com',password: 'Asd.1234',authToken:'',source: 0, userType:0}
          this.customCandidateAuthentication(this.currentUser)
          .subscribe(
              success => {
                setTimeout( () => {
                  this.registrationId = localStorage.getItem('registerId');
                  this.candidateDevice(this.registrationId);
                }, 500);
                  
                  //  this.deviceService.checkUserDevice();
                  //  this.authenticationService.getCandidateLastUpdate().subscribe();
                  
              }, err => {
                  console.error(err);
              }
          );
    }

    public customCandidateAuthentication(_newUser: any): any {
      let body = 'email=' + _newUser.email + '&password=' + _newUser.password;
      let headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      return this.http.post('https://apidev.alanajobs.com/secure-candidate/login_check', body, { headers: headers })
          .map((response: Response) => {
              let apiData = response.json();
              if (apiData && apiData.token) {
                  this.token ='Bearer '+apiData.token;
              }
          })
          .catch((error) => Observable.throw(error.json()));
    }

    private candidateDevice(_deviceId): void {
      const _data = {deviceToken: _deviceId,smartPhoneModel: 'asd'}

      this.http.post('https://apidev.alanajobs.com/secure-candidate/candidate/register-device', _data, {headers: new Headers ({ 'Authorization': this.token })})
      .map((data: any) => {
          let _version: string = data.response.version;
          console.log(data);
          return _version;
      })
      .subscribe();
    }


  
}
